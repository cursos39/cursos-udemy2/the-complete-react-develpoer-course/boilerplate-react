const path = require('path')

const PUBLIC_PATH = path.join(__dirname, 'public')
const ENTRY_APP_JS_PATH = './src/app.js'

module.exports = {
  entry: ENTRY_APP_JS_PATH,
  output: {
    path: PUBLIC_PATH,
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      loader: 'babel-loader',
      test: /\.js$/,
      exclude: /node_modules/
    }, {
      test: /\.s?css$/,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }]
  },
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    contentBase: PUBLIC_PATH
  }
}
